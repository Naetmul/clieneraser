package com.gmail.naetmul.clieneraser

import scala.util.{Failure, Success}
import scalafx.Includes._
import scalafx.application.Platform
import scalafx.event.ActionEvent
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.Node
import scalafx.scene.control.ButtonBar.ButtonData
import scalafx.scene.control._
import scalafx.scene.layout.GridPane

class LogInDialog extends Dialog[Auth] {
  title = StringResources.LogIn
  headerText = StringResources.LogInPrompt

  resizable = true

  // Button Region
  val logInButtonType = new ButtonType(StringResources.LogIn, ButtonData.OKDone)
  val cancelButtonType = ButtonType.Cancel

  dialogPane().buttonTypes = Seq(logInButtonType, cancelButtonType)

  // Content Region
  val messageLabel = new Label {
    text = StringResources.AuthorizationHint
    wrapText = true
    minHeight = 50
  }

  val idText = new TextField {
    promptText = StringResources.IdFieldHint
  }
  val passwordText = new PasswordField {
    promptText = StringResources.PasswordFieldHint
  }

  val progressBar = new ProgressBar {
    minWidth = 250
    progress = ProgressIndicator.INDETERMINATE_PROGRESS
    visible = false
  }

  // Layout Region
  dialogPane().content = new GridPane {
    hgap = 10
    vgap = 10
    padding = Insets(10)

    alignment = Pos.Center

    add(messageLabel, 0, 0, 2, 1)
    add(new Label(StringResources.Id), 0, 1)
    add(idText, 1, 1)
    add(new Label(StringResources.Password), 0, 2)
    add(passwordText, 1, 2)
    add(progressBar, 0, 3, 2, 1)
  }

  /* Note that if the type of logInButton is javafx.scene.Node (not scalafx.scene.Node),
   * then a compile error occurs at the filterEvent implicit conversion.
   */
  val logInButton: Node = dialogPane().lookupButton(logInButtonType)
  logInButton.disable = true

  logInButton.filterEvent(ActionEvent.Any) {
    ae: ActionEvent => {
      ae.eventType match {
        case ActionEvent.Action =>
          // If wrong ID or password is entered, then do not close the dialog.

          val auth = Auth(idText.text(), passwordText.text())

          messageLabel.text = StringResources.Authorizing
          progressBar.visible = true

          auth.authorize(_ match {
            case Success(authorized) if (authorized) =>
              this.result = auth
              this.close()
            case Success(authorized) if (!authorized) =>
              messageLabel.text = StringResources.AuthorizationFailed
              progressBar.visible = false
            case Failure(e) =>
              messageLabel.text = StringResources.authorizationError(e.toString)
              progressBar.visible = false
          })

          ae.consume()

        case _ => // Do nothing.
      }
    }
  }

  // void javafx.beans.value.ChangeListener<T>.changed(ObservableValue<? extends T> observable, T oldValue, T newValue)
  idText.text.onChange((_, _, newValue) => logInButton.disable = newValue.isEmpty || passwordText.text().isEmpty)
  passwordText.text.onChange((_, _, newValue) => logInButton.disable = idText.text().isEmpty || newValue.isEmpty)

  // Set focus on idText on start
  Platform.runLater(idText.requestFocus())

  // This will be converted into Option[Auth] (but will not be used since ActionEvent.Actions are all consumed.)
  resultConverter = {
    case `logInButtonType` => Auth(idText.text(), passwordText.text())
    case _ => null
  }
}
