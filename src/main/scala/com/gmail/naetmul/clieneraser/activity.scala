package com.gmail.naetmul.clieneraser

import org.joda.time.DateTime

trait Activity
case class Article(boardCode: String, id: Long, time: DateTime) extends Activity
case class Comment(boardCode: String, articleId: Long, commentId: Long, time: DateTime) extends Activity