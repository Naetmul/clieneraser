package com.gmail.naetmul.clieneraser

object StringResources {
  val ProgramName = "ClienEraser"

  val LogIn = "로그인"
  val LogOut = "로그아웃"

  val Id = "아이디"
  val Password = "비밀번호"

  val LogInPrompt = "클리앙에 로그인 합니다."

  val IdFieldHint = "아이디를 입력해 주세요."
  val PasswordFieldHint = "비밀번호를 입력해 주세요."

  def logInStatus(id: Option[String]): String = id match {
    case None => "로그인 되어 있지 않습니다."
    case Some(x) => s"아이디 ${x}(으)로 로그인 되었습니다."
  }

  val AuthorizationHint = "아이디와 비밀번호를 입력하고 나서 로그인 단추를 눌러주세요."
  val Authorizing = "로그인을 시도하고 있습니다."
  val AuthorizationFailed = "로그인에 실패했습니다.\n아이디나 비밀번호가 틀렸을 수 있습니다."
  def authorizationError(msg: String) = s"로그인 중에 오류가 발생했습니다.\n$msg"

  val Article = "글"
  val Comment = "댓글"
}
