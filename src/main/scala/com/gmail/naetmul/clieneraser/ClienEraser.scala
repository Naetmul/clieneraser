package com.gmail.naetmul.clieneraser

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.control._
import scalafx.scene.layout.{BorderPane, ColumnConstraints, GridPane, Priority}

object ClienEraser extends JFXApp {
  /* None: not logged in
   * Some(x): logged in as x
   */
  var auth: Option[Auth] = None

  /* UI components */
  val authLabel = new Label(StringResources.logInStatus(None))
  val authButton = new Button {
    text = StringResources.LogIn
    onAction = handle { logIn() }
  }

  /* Layout */
  stage = new PrimaryStage {
    title = StringResources.ProgramName

    minWidth = 400
    scene = new Scene {

      root = new BorderPane {
        padding = Insets(10)

        top = new GridPane {
          columnConstraints = Seq(
            new ColumnConstraints { hgrow = Priority.Always },
            new ColumnConstraints
          )

          hgap = 10

          add(authLabel, 0, 0)
          add(authButton, 1, 0)
        }

        center = new TabPane {
          padding = Insets(10)
          tabs = Seq(
            new Tab {
              text = StringResources.Article
              closable = false
            },
            new Tab {
              text = StringResources.Comment
              closable = false
            }
          )
        }
      }
    }
  }

  /** When LogIn button is pressed. */
  def logIn(): Unit = {
    val dialog = new LogInDialog

    /* Convert Option[DConvert[Auth, Auth => Auth]#S] with (DConvert[_, _]#S = Auth) into Option[Auth].
     * S is hidden, so the compiler does not know that DConvert[_, _]#S = Auth.
     */
    auth = dialog.showAndWait() match {
      case Some(v: Auth) => Some(v)
      case _ => None
    }

    authLabel.text = StringResources.logInStatus(auth map (_.id))
    auth match {
      case Some(v) =>
        authButton.text = StringResources.LogOut
        authButton.onAction = handle { logOut() }

        fetchActivities()
      case None => // Do nothing.
    }
  }

  /** When LogOut button is pressed. */
  def logOut(): Unit = {
    auth = None

    authLabel.text = StringResources.logInStatus(None)
    authButton.text = StringResources.LogIn
    authButton.onAction = handle { logIn() }
  }

  def fetchActivities(): Unit = {
    auth foreach (x => x.fetchActivities())
  }
}
