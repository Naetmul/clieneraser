package com.gmail.naetmul.clieneraser

import java.net.HttpCookie

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try
import scalafx.application.Platform
import scalaj.http.Http

case class Auth(id: String, password: String) {
  /* This cookie will save the log-in information. */
  var cookies: Seq[HttpCookie] = Seq.empty

  /** Log in.
    *
    * @param postTaskInMainThread This is like a callback function, which will be called in the main thread.
    */
  def authorize(postTaskInMainThread: Try[Boolean] => Unit): Unit = {
    val future: Future[Boolean] = Future {
      // HTTP POST
      val response = Http(Auth.LogInUrl)
        .timeout(Auth.ConnTimeoutMs, Auth.ReadTimeoutMs)
        .postForm(Seq(Auth.ParamKeyId -> id, Auth.ParamKeyPassword -> password))
        .asString

      // Save the cookies.
      cookies = response.cookies

      /* If a login is successful,
       * you are redirected to the previous page by "location.replace('http://www.clien.net..?nowlogin=1')".
       * Therefore, the following Boolean value indicates whether the login was successful.
       */
      response.body.contains(Auth.LogInCallback)
    } (ExecutionContext.global)

    future.onComplete {
      case result => Platform.runLater(postTaskInMainThread(result))
    } (ExecutionContext.global)
  }

  def fetchActivities(): Unit = {
    /*
    val response = Http(Auth.PointUrl)
      .timeout(Auth.ConnTimeoutMs, Auth.ReadTimeoutMs)
      .cookies(cookies)
      .param(Auth.ParamKeyPage, 1.toString)
      .asString
    */
  }
}

object Auth {
  val ConnTimeoutMs = 0 // infinite timeout
  val ReadTimeoutMs = 0 // infinite timeout

  private val LogInUrl = "https://www.clien.net/cs2/bbs/login_check.php"
  private val LogInCallback = "nowlogin=1"

  private val PointUrl = "http://www.clien.net/cs2/bbs/point.php"

  private val ParamKeyId = "mb_id"
  private val ParamKeyPassword = "mb_password"
  private val ParamKeyPage = "page"
}