val scalaFX = "org.scalafx" %% "scalafx" % "8.0.40-R8"
val jodaTime = "joda-time" % "joda-time" % "2.8.2"
val scalajHttp = "org.scalaj" %% "scalaj-http" % "2.2.0"
val jsoup = "org.jsoup" % "jsoup" % "1.8.3"

lazy val root = (project in file(".")).
  settings(
    name := "ClienEraser",
    version := "1.0",
    scalaVersion := "2.11.7",
    libraryDependencies ++= Seq(scalaFX, jodaTime, scalajHttp, jsoup),
    // Fork a new JVM for 'run' and 'test:run', to avoid JavaFX double initialization problems
    fork := true
  )